"""Main file for blopic module."""
from blopic.entrypoint import resize_image

if __name__ == '__main__':
    resize_image()  # pylint: disable=no-value-for-parameter
