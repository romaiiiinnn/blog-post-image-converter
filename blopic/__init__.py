"""Init file for blopic module."""
try:
    from blopic._version import __version__
except ImportError:
    __version__ = "undefined"
